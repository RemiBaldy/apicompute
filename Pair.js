class Pair {

    constructor(token_1, token_2, pancake_factory_contract, adrs, contract){
        this._token_1 = token_1;
        this._token_2 = token_2;
        this._pancake_factory_contract = pancake_factory_contract;
        this._adrs = adrs;
        this._contract = contract;
        this._reserves = 0;
    }

    get token_1() {
        return this._token_1;
    }

    get token_2() {
        return this._token_2;
    }

    get pancake_factory_contract() {
        return this._pancake_factory_contract;
    }

    get adrs() {
        return this._adrs;
    }

    get contract() {
        return this._contract;
    }
    get reserves() {
        return [this._reserves[0],this._reserves[1]];
    }
    async updateReserves(){
        this._reserves = await this.contract.getReserves();
    }

}
exports.Pair = Pair //


class InvPair{
    constructor(Pair){
        this.Pair = Pair;
    }

    async updateReserves(){
        await this.Pair.updateReserves();
    }

    get token_1() {
        return this.Pair._token_1;
    }

    get token_2() {
        return this.Pair._token_2;
    }

    get reserves() {
        return [this.Pair._reserves[1],this.Pair._reserves[0]];
    }

    get pancake_factory_contract() {
        return this.Pair._pancake_factory_contract;
    }

    get adrs() {
        return this.Pair._adrs;
    }

    get contract() {
        return this.Pair._contract;
    }

}
exports.InvPair = InvPair //




class FactoryPair{

    constructor() {
    }

    async create_pair(token_1, token_2, pancake_factory_contract, abi, ethersContract, account){

        var pair_adrs = await pancake_factory_contract.getPair(token_1, token_2);

        if (pair_adrs === '0x0000000000000000000000000000000000000000'){
            return 0;
        }
        var pair_contract = new ethersContract(pair_adrs, abi, account);
        var token0 = await pair_contract.token0();

        // Tokens in pancake-LPs are sometimes with wbnb first and sometimes not so :
        // token1 is wbnb

        if (token0.toLowerCase() === token_1.toLowerCase()){
            return new Pair(token_1, token_2, pancake_factory_contract, pair_adrs, pair_contract);
        }
        return new InvPair(new Pair(token_1, token_2, pancake_factory_contract, pair_adrs, pair_contract));
    }
}
exports.FactoryPair = FactoryPair //

