exports.apr = {
    'solar-solar-movr': 418,
    'solar-solar-usdc': 649,
    'solar-movr-usdc': 172,
    'solar-dai-usdc': 42.75,
    'solar-busd-usdc': 40.54,
    'solar-eth-usdc': 56.99,
    'solar-bnb-busd': 63.48,
    'solar-wbtc-usdc': 57.41,
    'solar-usdt-usdc': 47.03,
    'solar-mai-usdc': 74.83,
    'solar-mim-usdc': 65.31,
};