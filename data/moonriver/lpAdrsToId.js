exports.lpAdrsToId = {
    '0x7eDA899b3522683636746a2f3a7814e6fFca75e1': 'solar-solar-movr',
    '0xdb66BE1005f5Fe1d2f486E75cE3C50B52535F886': 'solar-solar-usdc',
    '0xe537f70a8b62204832B8Ba91940B77d3f79AEb81': 'solar-movr-usdc',
    '0xFE1b71BDAEE495dCA331D28F5779E87bd32FbE53': 'solar-dai-usdc',
    '0x384704557F73fBFAE6e9297FD1E6075FC340dbe5': 'solar-busd-usdc',
    '0xA0D8DFB2CC9dFe6905eDd5B71c56BA92AD09A3dC': 'solar-eth-usdc',
    '0xfb1d0D6141Fc3305C63f189E39Cc2f2F7E58f4c2': 'solar-bnb-busd',
    '0x83d7a3fc841038E8c8F46e6192BBcCA8b19Ee4e7': 'solar-wbtc-usdc',
    '0x2a44696DDc050f14429bd8a4A05c750C6582bF3b': 'solar-usdt-usdc',
    '0x55Ee073B38BF1069D5F1Ed0AA6858062bA42F5A9': 'solar-mai-usdc',
    '0x9051fB701d6D880800e397e5B5d46FdDfAdc7056': 'solar-mim-usdc',
};