const solarFactory = '0x049581aEB6Fe262727f290165C29BDAB065a1B68';

exports.tokens = {
    'SOLAR' : {adrs:'0x6bD193Ee6D2104F14F94E2cA6efefae561A4334B',factory:solarFactory, stableAdrs:'0xE3F5a90F9cb311505cd691a46596599aA1A0AD7D'},
    'MOVR' : {adrs:'0x98878B06940aE243284CA214f92Bb71a2b032B8A',factory:solarFactory, stableAdrs:'0xE3F5a90F9cb311505cd691a46596599aA1A0AD7D'},
    'ETH' : {adrs:'0x639A647fbe20b6c8ac19E48E2de44ea792c62c5C',factory:solarFactory, stableAdrs:'0xE3F5a90F9cb311505cd691a46596599aA1A0AD7D'},
    'BNB' : {adrs:'0x2bF9b864cdc97b08B6D79ad4663e71B8aB65c45c',factory:solarFactory, stableAdrs:'0x5D9ab5522c64E1F6ef5e3627ECCc093f56167818'},
    'WBTC' : {adrs:'0x6aB6d61428fde76768D7b45D8BFeec19c6eF91A8',factory:solarFactory, stableAdrs:'0xE3F5a90F9cb311505cd691a46596599aA1A0AD7D'},
}