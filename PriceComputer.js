class PriceComputer {

    constructor(BigNumber, ethersFormatUnits) {
        this.baseDecimal = 18;
        this.ethersFormatUnits = ethersFormatUnits;
        this.BigNumber = BigNumber;
    }

    convertReservesToJsNumber(reserves, decimals_1, decimals_2){

        return [parseFloat(this.ethersFormatUnits(reserves[0], decimals_1)), 
                parseFloat(this.ethersFormatUnits(reserves[1], decimals_2))]
    }

    computePriceTokenReserve(reserveToken, reservePair, decimalFirst, decimalScnd){
        let reserves = this.convertReservesToJsNumber([reserveToken,reservePair], decimalFirst, decimalScnd);
        return reserves[1]/reserves[0];
    }
}
exports.PriceComputer = PriceComputer //