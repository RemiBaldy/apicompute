class LpPricer {

    constructor(ethersContract, provider, lpInfos, tokensDecimal, ethersFormatUnits){
        this.provider = provider;
        this.ethersContract = ethersContract;
        this.lpInfos = lpInfos;
        this.tokensDecimal = tokensDecimal;
        this.ethersFormatUnits = ethersFormatUnits;
        this.lpFunctionsDict = this.getLpFunctionsDict();
        this.tokensFunctionsDict = this.getTokensFunctionsDict();
    }

    getLpFunctionsDict(){
        let lpFunctionsDict = {}

        for (let lp of this.lpInfos){
            lpFunctionsDict[lp.adrs] = new this.ethersContract(lp.adrs,
                [
                    "function totalSupply() external view returns (uint)",
                ],
                this.provider);
        }
        return lpFunctionsDict;
    }

    getTokensFunctionsDict(){
        let tokensFunctionsDict = {}

        for (const [adrs, decimal] of Object.entries(this.tokensDecimal)){
            tokensFunctionsDict[adrs] = new this.ethersContract(adrs,
                [
                    "function balanceOf(address account) external view returns (uint256)",
                ],
                this.provider);
        }
        return tokensFunctionsDict;
    }

    async getLpsTotalSupply(){
        let lpsTotalSupplyDict = {};
        for (let lp of this.lpInfos){
            let totalSupply = await this.lpFunctionsDict[lp.adrs].totalSupply();
            lpsTotalSupplyDict[lp.adrs] = parseFloat(this.ethersFormatUnits(totalSupply, lp.decimals));
        }
        return lpsTotalSupplyDict;
    }

    async getLpsBalances(){
        let lpsBalancesDict = {};
        for (let lp of this.lpInfos){
            let token0Balance = await this.tokensFunctionsDict[lp.token0].balanceOf(lp.adrs);
            let token1Balance = await this.tokensFunctionsDict[lp.token1].balanceOf(lp.adrs);
            let balances = {};
            balances[lp.token0] = token0Balance;
            balances[lp.token1] = token1Balance;
            lpsBalancesDict[lp.adrs]= balances;
        }
        return lpsBalancesDict;
    }

    convertBalancesToDollarsPrice(lpsBalancesDict, tokensPrices){
        let dollarsBalances = {};

        for (const [adrs, balances] of Object.entries(lpsBalancesDict)){
            let price = 0;
            for (const [tokenAdrs, balance] of Object.entries(balances)){
                let realBalance = parseFloat(this.ethersFormatUnits(balance, this.tokensDecimal[tokenAdrs]));
            
                if (tokensPrices[tokenAdrs]){
                    price += realBalance*tokensPrices[tokenAdrs];
                }
                else{
                    price += realBalance;
                }
            }
            dollarsBalances[adrs] = price;
        }
        return dollarsBalances;
    }

    async computeLpPrice(tokensPrices){
        let lpsTotalSupplyDict = await this.getLpsTotalSupply();
        let lpsBalancesDict = await this.getLpsBalances();
        let dollarsBalances = this.convertBalancesToDollarsPrice(lpsBalancesDict, tokensPrices);

        let lpPrices = {};

        for (const [adrs, totalSupply] of Object.entries(lpsTotalSupplyDict)){
            lpPrices[adrs] = dollarsBalances[adrs]/totalSupply;
        }

        return lpPrices;
    }

}
exports.LpPricer = LpPricer //