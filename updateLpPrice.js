const ethers = require("ethers");
// const axiosPost = require('axios').post;
const fs = require("fs");

const TokenPriceComputerConstruct = require('./TokenPriceComputer').TokenPriceComputer;
const LpPricerConstruct = require('./LpPricer').LpPricer;
// const PosterConstruct = require('./Poster').Poster;
const SaverConstruct = require('./Saver').Saver;

const blockChainAdresses = require('./blockChainAdresses');

// const tokensInfos = require('./data/polygon/polygonTokens').tokens;
// const tokensDecimal = require('./data/polygon/polygonTokensDecimal').tokensDecimal;
// const lps = require('./data/polygon/polygonLpInfo').polygonLps;
// const lpAdrsToId = require('./data/polygon/polygonLpAdrsToId').polygonLpAdrsToId;

const tokensInfos = require('./data/moonriver/tokens').tokens;
const tokensDecimal = require('./data/moonriver/tokensDecimal').tokensDecimal;
const lps = require('./data/moonriver/lpInfo').lps;
const lpAdrsToId = require('./data/moonriver/lpAdrsToId').lpAdrsToId;


// let provider = new ethers.providers.JsonRpcProvider(blockChainAdresses.providerAdrs);
let provider = new ethers.providers.WebSocketProvider(blockChainAdresses.providerAdrs);

let pingTimeout = null;
let keepAliveInterval = null;
const EXPECTED_PONG_BACK = 15000;
const KEEP_ALIVE_CHECK_INTERVAL = 7500;

provider._websocket.on('open', () => {
  keepAliveInterval = setInterval(() => {
    // console.log('Checking if the connection is alive, sending a ping')

    provider._websocket.ping();

    // Use `WebSocket#terminate()`, which immediately destroys the connection,
    // instead of `WebSocket#close()`, which waits for the close timer.
    // Delay should be equal to the interval at which your server
    // sends out pings plus a conservative assumption of the latency.
    pingTimeout = setTimeout(() => {
      provider._websocket.terminate();
    }, EXPECTED_PONG_BACK)
  }, KEEP_ALIVE_CHECK_INTERVAL)

  // TODO: handle contract listeners setup + indexing
})

provider._websocket.on('close', () => {
    // console.log('The websocket connection was closed')
    clearInterval(keepAliveInterval);
    clearTimeout(pingTimeout);
    startConnection();
})

provider._websocket.on('pong', () => {
    // console.log('Received pong, so connection is alive, clearing the timeout')
    clearInterval(pingTimeout);
})

let TokenPriceComputer = new TokenPriceComputerConstruct(ethers.Contract, provider, tokensDecimal, ethers.BigNumber, ethers.utils.formatUnits);
let LpPricer = new LpPricerConstruct(ethers.Contract, provider, lps, tokensDecimal, ethers.utils.formatUnits);
// let Poster = new PosterConstruct(axiosPost, lpAdrsToId);
let Saver = new SaverConstruct(lpAdrsToId, fs);

const SLEEP_TIME = 60000;


try {
    (async () => {
        await TokenPriceComputer.setTokenStablePairs(tokensInfos, TokenPriceComputer.getFactoriesContract(tokensInfos));
        while(true){            

            await TokenPriceComputer.updateReserves();
            const tokensPrices = TokenPriceComputer.computePrices();
            // console.log(tokensPrices);

            const lpPrices = await LpPricer.computeLpPrice(tokensPrices);
            console.log(lpPrices);

            Saver.saveLpPrices(lpPrices);
            // await Poster.formatLpPricesAndPost(lpPrices);


            await new Promise(r => setTimeout(() => r(), SLEEP_TIME));
        }
    })();
} catch (error) {
    console.error(error);
}


