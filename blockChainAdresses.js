// BSC testnet
// const providerAdrs = "https://data-seed-prebsc-2-s1.binance.org:8545/";

// POLYGON
// const providerAdrs = "wss://ws-matic-mainnet.chainstacklabs.com";
// const providerAdrs = "wss://rpc-mainnet.matic.quiknode.pro"
// const providerAdrs = "ws://localhost:8545";

// Moonriver
// const providerAdrs = "https://rpc.moonriver.moonbeam.network";
const providerAdrs = "wss://wss.moonriver.moonbeam.network";

exports.providerAdrs = providerAdrs;

