const FactoryPairConstructor = require('./Pair').FactoryPair;
const PriceComputerConstructor = require('./PriceComputer').PriceComputer;

class TokenPriceComputer {

    constructor(ethersContract, provider, tokensDecimal, BigNumber, ethersFormatUnits){
        this.provider = provider;
        this.ethersContract = ethersContract;
        this.tokensDecimal = tokensDecimal;
        this.FactoryPair = new FactoryPairConstructor();
        this.PriceComputer = new PriceComputerConstructor(BigNumber, ethersFormatUnits);
    }

    computePrices(){
        let pricesDict = {};
        for (const [key, pair] of Object.entries(this.tokenStablePairs)){
            let reserves = pair.reserves;
            pricesDict[key] = this.PriceComputer.computePriceTokenReserve(reserves[0], reserves[1], this.tokensDecimal[pair.token_1], this.tokensDecimal[pair.token_2]);
        }
        return pricesDict;
    }

    async updateReserves(){
        let promisesUpdate = [];

        for (const [key, pair] of Object.entries(this.tokenStablePairs)){
            promisesUpdate.push(pair.updateReserves());
        }
        await Promise.all(promisesUpdate);
    }


    getFactoriesContract(tokensInfos){
        let factoryAdrsSet = new Set();

        for (const [key, value] of Object.entries(tokensInfos)){
            factoryAdrsSet.add(value['factory']);
        }

        let factoriesContractDict = {};

        for (let factoryAdrs of factoryAdrsSet){

            factoriesContractDict[factoryAdrs] = new this.ethersContract(factoryAdrs,
                [
                    "function getPair(address tokenA, address tokenB) external view returns (address pair)"
                ],
                this.provider);
        }
        return factoriesContractDict
    }

    async setTokenStablePairs(tokensInfos, factoriesContractDict){       
        this.tokenStablePairs = {};

        let pairAbi = ["function token0() external view returns (address)","function getReserves() external view returns (uint112 reserve0, uint112 reserve1, uint32 blockTimestampLast)"];

        for (const [key, value] of Object.entries(tokensInfos)){
            this.tokenStablePairs[value['adrs']] = await this.FactoryPair.create_pair(value['adrs'], value['stableAdrs'], factoriesContractDict[value['factory']], pairAbi, this.ethersContract, this.provider);
        }            
    }

}
exports.TokenPriceComputer = TokenPriceComputer //