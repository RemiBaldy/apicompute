class Saver {
    constructor(lpAdrsToId, fs){
        this.lpAdrsToId = lpAdrsToId;
        this.fs = fs;
    }

    convertAdrsToId(lpPrices){
        let lpPricesById = {};
        for (const [adrs, price] of Object.entries(lpPrices)){
            lpPricesById[this.lpAdrsToId[adrs]] = price;
        }
        return lpPricesById;
    }

    saveLpPrices(lpPrices){
        let lpPricesById = this.convertAdrsToId(lpPrices);
        this.fs.writeFileSync('./data/lpPrices.json', JSON.stringify(lpPricesById) , 'utf-8');
    }

    saveAprApy(aprApyByIdDict){
        this.fs.writeFileSync('./data/apys.json', JSON.stringify(aprApyByIdDict) , 'utf-8');
    }

}
exports.Saver = Saver //