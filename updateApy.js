const aprDict = require('./data/moonriver/apr').apr;

const fs = require("fs");

const periodsCount = 365;

const SaverConstruct = require('./Saver').Saver;
let Saver = new SaverConstruct({}, fs);

function convertAprToApy(apr){
  return (1 + (apr / periodsCount))**(periodsCount) - 1
}

let apyAprDict = {}
for (const [id, apr] of Object.entries(aprDict)){
  apyAprDict[id] = {"totalApy":convertAprToApy(apr/100), "totalApr":apr/100};
}
Saver.saveAprApy(apyAprDict);
