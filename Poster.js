class Poster {
    constructor(axiosPost, lpAdrsToId){
        this.axiosPost = axiosPost;
        this.apiUrlLpPrices = 'http://localhost:1234/api/lpPrices/replaceOrCreate';
        this.lpAdrsToId = lpAdrsToId;
    }

    async formatLpPricesAndPost(lpPrices){
        for (const [lpAdrs, lpPrice] of Object.entries(lpPrices)){
            this.postLpPrice({price:lpPrice, id:this.lpAdrsToId[lpAdrs]});
        }
    }

    postLpPrice(lpPrice){
        this.axiosPost(this.apiUrlLpPrices, lpPrice)
        .then(res => {
          console.log(`statusCode: ${res.status}`)
        })
        .catch(error => {
          console.error("error")
        })
    }
}
exports.Poster = Poster //